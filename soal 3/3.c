#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(void){
    pid_t child_id, child_id1, child_id2, child_id3, child_id4, child_id5, child_id6, child_id7;
    int status;

    //3a
    child_id = fork();
    if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", "/home/mcharisyah/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    } else 
        while((wait(&status)) > 0);
        
    child_id1 = fork();
    if(child_id1 < 0) exit(EXIT_FAILURE);

    if(child_id1 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/mcharisyah/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    } else while((wait(&status)) > 0);
        
    //3b
    child_id2 = fork();

    if(child_id2 < 0) exit(EXIT_FAILURE);

    if(child_id2 == 0){
        char *argv[] = {"unzip", "-oq", "/home/mcharisyah/Downloads/animal.zip", "-d", "/home/mcharisyah/modul2/", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);
    
    //3c
    child_id3 = fork();
    if(child_id3 < 0) exit(EXIT_FAILURE);

    if(child_id3 == 0){
        execl("/usr/bin/find", "find", "/home/mcharisyah/modul2/animal/", "-type", "f", "-name", "*darat*", "-exec", "mv", "-t", "/home/mcharisyah/modul2/darat", "{}", "+", (char *) NULL);
    } else while((wait(&status)) > 0);
                
    child_id4 = fork();
    if(child_id4 < 0) exit(EXIT_FAILURE);

    if(child_id4 == 0){
        sleep(3);
        execl("/usr/bin/find", "find", "/home/mcharisyah/modul2/animal/", "-type", "f", "-name", "*air*", "-exec", "mv", "-t", "/home/mcharisyah/modul2/air/", "{}", "+", (char *) NULL);
    } else while(wait((&status)) > 0);
        
    //remove file in dir: animal
    child_id5 = fork();

    if(child_id5 < 0) exit(EXIT_FAILURE);

    if(child_id5 == 0) 
        execl("/bin/sh", "sh", "-c", "rm -f /home/mcharisyah/modul2/animal/*", (char *) NULL);
    else while(wait((&status)) > 0);
        
    //remove bird
    child_id6 = fork();
    if(child_id6 < 0) exit(EXIT_FAILURE);

    if (child_id6 == 0){
        execl("/bin/sh", "sh", "-c", "rm -f /home/mcharisyah/modul2/darat/*bird*", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);

    //input file name format: uid_uid permission_filename
    child_id7 = fork();
    if(child_id7 < 0) exit(EXIT_FAILURE);
  
    if (child_id7 == 0){
        execl("/bin/sh", "sh", "-c", "ls -la /home/mcharisyah/modul2/air | awk 'NR > 3 {print $3\"_\"substr($1,2,2)\"_\"$9}' > /home/mcharisyah/modul2/air/list.txt", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
}
